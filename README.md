# List of Games #

Projekt lista gier

### Setup project ###

* Open solution
* Solution -> Right-click -> SetStartUp Project
* Multiple startup projects: -ListOfGames -> Start -ListOfGames.Web -> Start without debugging
* ListOfGames.Web -> Right-click -> Start Options
* Specific page: index.html
* ListOfGames -> Right-click -> Properties
* Web -> Don't open a page...

### Update database ###

Before run application open "Package Manager Console" and paste code below

```
update-database -ProjectName ListOfGames.Domain -StartUpProjectName ListOfGames
```