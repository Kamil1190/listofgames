﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models;
using ListOfGames.Domain.Models.Dto;

namespace ListOfGames.Controllers
{
    [RoutePrefix("api/games")]
    [EnableCors(origins: "http://localhost:53844", headers: "*", methods: "*")]
    public class GamesController : ApiController
    {
        private readonly IGameRepository _repository;

        public GamesController(IGameRepository repository)
        {
            _repository = repository;
        }


        // GET: api/Games
        public IHttpActionResult Get()
        {
            return Ok(_repository.GetAllGames.Select(x => Mapper.Map<GameDto>(x)));
        }

        // GET: api/Games/5
        public IHttpActionResult Get(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var game = _repository.GetGame(id);
            if (game == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<GameDto>(game));
        }

        // POST: api/Games
        public IHttpActionResult Post([FromBody]GameDto gameDto)
        {
            if (!ModelState.IsValid || gameDto == null)
            {
                return BadRequest();
            }
            gameDto.Id = 0;
            Game game = Mapper.Map<Game>(gameDto);
            game.Platform = _repository.GetAllPlatforms.Where(x => gameDto.PlatformId.Any(y => y == x.Id)).ToList();
            game = _repository.SaveGame(game);
            if (game == null)
            {
                return InternalServerError();
            }
            return Ok(Mapper.Map<GameDto>(game));
        }

        // PUT: api/Games/5
        public IHttpActionResult Put(int id, [FromBody]GameDto gameDto)
        {
            if (!ModelState.IsValid || id == 0 || gameDto == null)
            {
                return BadRequest();
            }
            Game game = Mapper.Map<Game>(gameDto);
            game.Platform = _repository.GetAllPlatforms.Where(x => gameDto.PlatformId.Any(y => y == x.Id)).ToList();
            game = _repository.UpdateGame(id,game);
            if (game == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<GameDto>(game));
        }

        // DELETE: api/Games/5
        public IHttpActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            Game gameDb = _repository.DeleteGame(id);
            if (gameDb == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<GameDto>(gameDb));
        }

        /// <summary>
        /// This action returns list GameType enums
        /// </summary>
        /// <returns>List with values and names</returns>
        [HttpGet, Route("types")]
        public IHttpActionResult GetGameTypes()
        {
            Array values = Enum.GetValues(typeof(Gametype));
            List<GameTypeDto> gametypes = new List<GameTypeDto>();
            foreach (Gametype val in values)
            {
                gametypes.Add(new GameTypeDto
                {
                    value = (int) val,
                    name = Enum.GetName(typeof(Gametype), val)
                });
            }
            return Ok(gametypes);

        }
    }
}
