﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models.Dto;

namespace ListOfGames.Controllers
{
    [RoutePrefix("api/platforms")]
    [EnableCors(origins: "http://localhost:53844", headers: "*", methods: "*")]
    public class PlatformsController : ApiController
    {
        private IGameRepository _repository;

        public PlatformsController(IGameRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Platforms
        public IHttpActionResult Get()
        {
            return Ok(_repository.GetAllPlatforms.Select(x => Mapper.Map<PlatformDto>(x)));
        }

        public IHttpActionResult Get(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var platform = _repository.GetPlatform(id);
            if (platform == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<PlatformDto>(platform));
        }

        // POST: api/Platforms
        public IHttpActionResult Post([FromBody]PlatformDto platformDto)
        {
            if (!ModelState.IsValid || platformDto == null)
            {
                return BadRequest();
            }
            platformDto.Id = 0;
            Platform platform = Mapper.Map<Platform>(platformDto);
            platform = _repository.SavePlatform(platform);
            if (platform == null)
            {
                return InternalServerError();
            }
            return Ok(Mapper.Map<PlatformDto>(platform));
        }

        // PUT: api/Platforms/5
        public IHttpActionResult Put(int id, [FromBody]PlatformDto platformDto)
        {
            if (!ModelState.IsValid || id == 0|| platformDto == null)
            {
                return BadRequest();
            }
            Platform platform = Mapper.Map<Platform>(platformDto);
            platform = _repository.UpdatePlatform(id, platform);
            if (platform == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<PlatformDto>(platform));
        }

        // DELETE: api/Platforms/5
        public IHttpActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            Platform platform = _repository.DeletePlatform(id);
            if (platform == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<PlatformDto>(platform));
        }
    }
}
