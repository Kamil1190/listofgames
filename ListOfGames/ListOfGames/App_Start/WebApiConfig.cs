﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using AutoMapper;
using ListOfGames.App_Start;
using ListOfGames.Domain.Abstract;
using Newtonsoft.Json.Serialization;

namespace ListOfGames
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            //REGISTER NINJECT
            config.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(new Ninject.Web.Common.Bootstrapper().Kernel);

            //AUTOMAPPER
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());

            //CoRS
            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //JSON FORMATTER
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
