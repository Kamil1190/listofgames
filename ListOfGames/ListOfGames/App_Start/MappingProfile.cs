﻿using System;
using System.Linq;
using AutoMapper;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models;
using ListOfGames.Domain.Models.Dto;

namespace ListOfGames.App_Start
{
    /// <summary>
    /// This class contains all mapps using by AutoMapper
    /// </summary>
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Game, GameDto>().ForMember(dto => dto.PlatformId, opt => opt.MapFrom(x => x.Platform.Select(y => y.Id)));
            CreateMap<Game, Game>().ForMember(opt1 => opt1.Id, opt2 => opt2.Ignore());
            CreateMap<GameDto, Game>().ForMember(opt => opt.Type, dto => dto.MapFrom(x => Enum.GetName(typeof(Gametype), x.Type)));

            CreateMap<Platform, Platform>().ForMember(opt1 => opt1.Id, opt2 => opt2.Ignore());
            CreateMap<PlatformDto, Platform>();
            CreateMap<Platform, PlatformDto>();
        }
    }
}