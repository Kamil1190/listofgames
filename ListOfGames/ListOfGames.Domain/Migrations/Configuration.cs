using System.Collections.Generic;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Fake;

namespace ListOfGames.Domain.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ListOfGames.Domain.ApplicationDbContext>
    {
        //enable-migrations -ProjectName ListOfGames.Domain -StartUpProjectName ListOfGames -Verbose
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        /// <summary>
        /// Initialize data in database using local repo
        /// </summary>
        /// <param name="context">This is dbContext</param>
        protected override void Seed(ListOfGames.Domain.ApplicationDbContext context)
        {
            List<Game> games = new FakeGameRepository().GetAllGames().ToList();
            context.Games.AddRange(games);
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
