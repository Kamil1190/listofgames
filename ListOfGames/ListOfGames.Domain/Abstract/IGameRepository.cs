﻿using System;
using System.Collections.Generic;

using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models.Dto;

namespace ListOfGames.Domain.Abstract
{
    /// <summary>
    /// Interface using to wrapp repository
    /// </summary>
    public interface IGameRepository
    {
        IEnumerable<Game> GetAllGames { get; }
        Game GetGame(int id);
        Game SaveGame(Game game);
        Game UpdateGame(int id, Game game);
        Game DeleteGame(int id);

        IEnumerable<Platform> GetAllPlatforms { get; }
        Platform GetPlatform(int id);
        Platform SavePlatform(Platform platform);
        Platform UpdatePlatform(int id, Platform platform);
        Platform DeletePlatform(int id);
    }
}
