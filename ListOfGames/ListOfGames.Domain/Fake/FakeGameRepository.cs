﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models;
using ListOfGames.Domain.Models.Dto;

namespace ListOfGames.Domain.Fake
{
    /// <summary>
    /// Contains fake object for testing
    /// </summary>
    public class FakeGameRepository
    {
        /// <summary>
        /// List representing games
        /// </summary>
        private IEnumerable<Game> games;
        /// <summary>
        /// List representing platforms
        /// </summary>
        private IEnumerable<Platform> platforms;

        public FakeGameRepository()
        {
            platforms = new List<Platform>
            {
                new Platform
                {
                    Id = 1,
                    Name = "PC"
                },
                new Platform
                {
                    Id = 2,
                    Name = "PS4"
                },
                new Platform
                {
                    Id = 3,
                    Name = "XBOXONE"
                },
                new Platform
                {
                    Id = 4,
                    Name = "Android"
                },
            };
            games = new List<Game>
            {

                new Game {
                    Id = 1,
                    Title = "Wiedzmin 3",
                    Type = Gametype.Rpg,
                    Price = 100,
                    Description = "Wiedźmin 3: Dziki Gon to opracowana przez zespół CD Projekt RED gra cRPG. Ponownie wcielamy się w Geralta z Rivii",
                    Platform = platforms.Where(p => p.Name.Equals("PC") || p.Name.Equals("PS4") || p.Name.Equals("XBOXONE")).ToList()
                },
                new Game {
                    Id = 2,
                    Title = "Grand Theft Auto V",
                    Type = Gametype.Akcji,
                    Price = 169.99,
                    Description = "Kolejna odsłona bestsellerowej gangsterskiej serii studia Rockstar North, rozgrywająca się w olbrzymim, otwartym świecie miasta Los Santos",
                    Platform = platforms.Where(p => p.Name.Equals("PC") || p.Name.Equals("PS4") || p.Name.Equals("XBOXONE")).ToList()
                },
                new Game {
                    Id = 3,
                    Title = "Euro Truck Simulator 2",
                    Type = Gametype.Symulacje,
                    Price = 59.99,
                    Description = "Następna część znanego symulatora samochodów ciężarowych z akcją rozgrywaną na terenie Europy, autorstwa firmy SCS Software.",
                    Platform = platforms.Where(p => p.Name.Equals("PC")).ToList()
                },
                new Game {
                    Id = 4,
                    Title = "Pokemon GO",
                    Type = Gametype.Rpg,
                    Price = 0,
                    Description = "Pokemon GO to wydana na urządzenia mobilne z systemem Android nietypowa gra RPG",
                    Platform = platforms.Where(p => p.Name.Equals("Android")).ToList()
                },
            };
        }


        /// <summary>
        /// This method returns all Games from this repository
        /// </summary>
        public IEnumerable<Game> GetAllGames()
        {

            return games;
            //return new List<Game>
            //            {
            //                new Game {
            //                    Id = 1,
            //                    Title = "Wiedzmin 3",
            //                    GameType = Gametype.Rpg,
            //                    Price = 100,
            //                    Description = "Wiedźmin 3: Dziki Gon to opracowana przez zespół CD Projekt RED gra cRPG. Ponownie wcielamy się w Geralta z Rivii"
            //                },
            //                new Game {
            //                    Id = 2,
            //                    Title = "Grand Theft Auto V",
            //                    GameType = Gametype.Akcji,
            //                    Price = 169.99,
            //                    Description = "Kolejna odsłona bestsellerowej gangsterskiej serii studia Rockstar North, rozgrywająca się w olbrzymim, otwartym świecie miasta Los Santos"
            //                },
            //                new Game {
            //                    Id = 3,
            //                    Title = "Euro Truck Simulator 2",
            //                    GameType = Gametype.Symulacje,
            //                    Price = 59.99,
            //                    Description = "Następna część znanego symulatora samochodów ciężarowych z akcją rozgrywaną na terenie Europy, autorstwa firmy SCS Software."
            //                },
            //                new Game {
            //                    Id = 4,
            //                    Title = "Pokemon GO",
            //                    GameType = Gametype.Rpg,
            //                    Price = 0,
            //                    Description = "Pokemon GO to wydana na urządzenia mobilne z systemem Android nietypowa gra RPG"
            //                },
            //            };
        }
        /// <summary>
        /// This method returns all Platforms from this repository 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Platform> GetAllPlatforms()
        {
            return platforms;
        }
}
}
