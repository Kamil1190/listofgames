﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListOfGames.Domain.Entites
{
    /// <summary>
    /// Entity model represents a platform, has many-to-many relation with game
    /// </summary>
    public class Platform
    {
        public Platform()
        {
            this.Games = new HashSet<Game>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}
