﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListOfGames.Domain.Models;

namespace ListOfGames.Domain.Entites
{
    /// <summary>
    /// Entity model represents a game, has many-to-many relation with platform
    /// </summary>
    public class Game
    {
        public Game()
        {
            this.Platform = new HashSet<Platform>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public Gametype Type { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Platform> Platform { get; set; }
    }
}
