﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models.Dto;

namespace ListOfGames.Domain.Repository
{
    /// <summary>
    /// service using to wrap Entity Framework context
    /// </summary>
    public class EFGameRepository : IGameRepository
    {
        private ApplicationDbContext db;

        public EFGameRepository()
        {
            db = new ApplicationDbContext();
        }

        public IEnumerable<Game> GetAllGames
        {
            get { return db.Games; }
        }

        public Game GetGame(int id)
        {
            return db.Games.Find(id);
        }

        public Game SaveGame(Game game)
        {
            db.Games.Add(game);
            db.SaveChanges();
            return game;
        }

        public Game UpdateGame(int id, Game game)
        {
            Game gameDb = db.Games.Find(id);
            if (gameDb != null)
            {
                gameDb.Title = game.Title;
                gameDb.Type = game.Type;
                gameDb.Price = game.Price;
                gameDb.Description = game.Description;
                gameDb.Platform.Clear();
                gameDb.Platform = game.Platform;

                //Mapper.Map(game, gameDb);
                db.SaveChanges();
            }
            return gameDb;
        }

        public Game DeleteGame(int id)
        {
            Game gameDb = db.Games.Find(id);
            if (gameDb != null)
            {
                db.Games.Remove(gameDb);
                db.SaveChanges();
            }
            return gameDb;
        }

        //PLATFORMS

        public IEnumerable<Platform> GetAllPlatforms
        {
            get { return db.Platforms; }
        }

        public Platform GetPlatform(int id)
        {
            return db.Platforms.Find(id);
        }

        public Platform SavePlatform(Platform platform)
        {
            db.Platforms.Add(platform);
            db.SaveChanges();
            return platform;
        }

        public Platform UpdatePlatform(int id, Platform platform)
        {
            Platform platformDb = db.Platforms.Find(id);
            if (platformDb != null)
            {
                Mapper.Map(platform, platformDb);
                db.SaveChanges();
            }
            return platformDb;
        }

        public Platform DeletePlatform(int id)
        {
            Platform platformDb = db.Platforms.Find(id);
            if (platformDb != null)
            {
                db.Platforms.Remove(platformDb);
                db.SaveChanges();
            }
            return platformDb;
        }

    }
}
