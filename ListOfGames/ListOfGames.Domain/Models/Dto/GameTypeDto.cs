﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListOfGames.Domain.Models.Dto
{
    public class GameTypeDto
    {
        public int value { get; set; }
        public string name { get; set; }
    }
}
