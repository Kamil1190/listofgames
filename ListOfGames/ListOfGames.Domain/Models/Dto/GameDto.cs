﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListOfGames.Domain.Models.Dto
{
    public class GameDto
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public int Type { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public List<int> PlatformId { get; set; }
    }
}
