﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListOfGames.Domain.Models.Dto
{
    public class PlatformDto
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
