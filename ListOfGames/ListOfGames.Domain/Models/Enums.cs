﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListOfGames.Domain.Models
{
    public enum Gametype
    {
        Akcji,
        Zręcznościowe,
        Bijatyki,
        Rpg,
        Strategiczne,
        Przygodowe,
        Sportowe,
        Wyścigi,
        Symulacje,
        Logiczne,
        Towarzyskie,
        Mmo
    }

}
