﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using AutoMapper;
using ListOfGames.App_Start;
using ListOfGames.Controllers;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Fake;
using ListOfGames.Domain.Models.Dto;
using ListOfGames.Tests.Compares;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ListOfGames.Tests
{
    /// <summary>
    /// TEST FOR GamesController
    /// </summary>
    [TestClass]
    public class GamesUnitTest
    {
        /// <summary>
        /// Initialize data for test
        /// </summary>
        /// <param name="context">The context of test.</param>
        [ClassInitialize]
        public static void Init(TestContext context)
        {
            //INITIALIZE AUTOMAPPER FOR TEST
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());

        }
        /// <summary>
        /// Test for get all games
        /// </summary>
        [TestMethod]
        public void Can_Get_All_Items()
        {
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            FakeGameRepository fakeGameRepository = new FakeGameRepository();
            var beforeOrginalItems = fakeGameRepository.GetAllGames();
            var beforeDtoItems = beforeOrginalItems.Select(x => Mapper.Map<GameDto>(x));
            mock.Setup(m => m.GetAllGames).Returns(beforeOrginalItems);
            GamesController controller = new GamesController(mock.Object);

            //run
            var result = controller.Get();

            //assert
            var response = result as OkNegotiatedContentResult<IEnumerable<GameDto>>;
            Assert.IsNotNull(response);
            mock.Verify(m => m.GetAllGames);    //if GetAllGames was used
            CollectionAssert.AreEqual(beforeDtoItems.ToList(), response.Content.ToList(), new GameDtoComparer());
        }

        /// <summary>
        /// Test for get single game
        /// </summary>
        [TestMethod]
        public void Can_Get_Item()
        {
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            int gameId = 1;
            FakeGameRepository fakeGameRepository = new FakeGameRepository();
            var beforeOrginalItem = fakeGameRepository.GetAllGames().SingleOrDefault(x => x.Id == gameId);
            var beforeDtoItem = Mapper.Map<GameDto>(beforeOrginalItem);
            mock.Setup(m => m.GetGame(It.IsAny<int>())).Returns(beforeOrginalItem);
            GamesController controller = new GamesController(mock.Object);

            //run
            var result = controller.Get(gameId);

            //assert
            var response = result as OkNegotiatedContentResult<GameDto>;
            Assert.IsNotNull(response);
            mock.Verify(m => m.GetGame(It.Is<int>(i => i == gameId)));
            if (new GameDtoComparer().Compare(beforeDtoItem, response.Content) != 0)
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// Test for saving new game
        /// </summary>
        [TestMethod]
        public void Can_Save_Game()
        {
            Game gameToAdd = new FakeGameRepository().GetAllGames().FirstOrDefault();
            GameDto gameToAddDto = Mapper.Map<GameDto>(gameToAdd);

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.SaveGame(It.IsAny<Game>())).Returns(gameToAdd);
            GamesController controller = new GamesController(mock.Object);

            //run
            var result = controller.Post(gameToAddDto);

            //assert
            var response = result as OkNegotiatedContentResult<GameDto>;
            Assert.IsNotNull(response);
            mock.Verify(m => m.SaveGame(It.IsAny<Game>()));
        }

        /// <summary>
        /// Test for saving new game when model error occurred
        /// </summary>
        [TestMethod]
        public void Cannot_Save_Game_Model_Error()
        {
            Game gameToAdd = new FakeGameRepository().GetAllGames().FirstOrDefault();
            GameDto gameToAddDto = Mapper.Map<GameDto>(gameToAdd);

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.SaveGame(It.IsAny<Game>())).Returns(gameToAdd);
            GamesController controller = new GamesController(mock.Object);
            controller.ModelState.AddModelError("error", "error");

            //run
            var result = controller.Post(gameToAddDto);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            mock.Verify(m => m.SaveGame(It.IsAny<Game>()), Times.Never);
        }

        /// <summary>
        /// Test for update game
        /// </summary>
        [TestMethod]
        public void Can_Update_Game()
        {
            Game gameToUpdate = new FakeGameRepository().GetAllGames().FirstOrDefault();
            GameDto gameToAddDto = Mapper.Map<GameDto>(gameToUpdate);
            int idGameToAdd = gameToAddDto.Id;

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.UpdateGame(It.IsAny<int>(), It.IsAny<Game>())).Returns(gameToUpdate);
            GamesController controller = new GamesController(mock.Object);


            //run
            var result = controller.Put(idGameToAdd, gameToAddDto);

            //assert
            var response = result as OkNegotiatedContentResult<GameDto>;
            Assert.IsNotNull(response);
            mock.Verify(m => m.UpdateGame(It.Is<int>(i => i != 0), It.IsAny<Game>()));
        }

        /// <summary>
        /// Test for update game when model error occurred
        /// </summary>
        [TestMethod]
        public void Cannot_Update_Game_Model_Error()
        {
            Game gameToUpdate = new FakeGameRepository().GetAllGames().FirstOrDefault();
            GameDto gameToAddDto = Mapper.Map<GameDto>(gameToUpdate);
            int idGameToAdd = gameToAddDto.Id;

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.UpdateGame(It.IsAny<int>(), It.IsAny<Game>())).Returns(gameToUpdate);
            GamesController controller = new GamesController(mock.Object);
            controller.ModelState.AddModelError("error", "error");

            //run
            var result = controller.Put(idGameToAdd, gameToAddDto);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            mock.Verify(m => m.UpdateGame(It.Is<int>(i => i == idGameToAdd), It.IsAny<Game>()), Times.Never);
        }

        /// <summary>
        /// Test for update game when bad id occurred
        /// </summary>
        [TestMethod]
        public void Cannot_Update_Game_Bad_Id()
        {
            Game gameToUpdate = new FakeGameRepository().GetAllGames().FirstOrDefault();
            GameDto gameToAddDto = Mapper.Map<GameDto>(gameToUpdate);
            int idGameToAdd = 0;

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.UpdateGame(It.IsAny<int>(), It.IsAny<Game>())).Returns(gameToUpdate);
            GamesController controller = new GamesController(mock.Object);


            //run
            var result = controller.Put(idGameToAdd, gameToAddDto);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            mock.Verify(m => m.UpdateGame(It.Is<int>(i => i == idGameToAdd), It.IsAny<Game>()), Times.Never);
        }

        /// <summary>
        /// Test for deleting game
        /// </summary>
        [TestMethod]
        public void Can_Delete_Game()
        {
            Game gameToDelete = new FakeGameRepository().GetAllGames().FirstOrDefault();

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            GamesController controller = new GamesController(mock.Object);

            //run
            var result = controller.Delete(gameToDelete.Id);

            //assert
            var response = result as OkNegotiatedContentResult<GameDto>;
            mock.Verify(m => m.DeleteGame(gameToDelete.Id));
        }
    }
}
