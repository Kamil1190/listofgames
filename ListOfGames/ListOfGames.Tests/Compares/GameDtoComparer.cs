﻿using System.Collections.Generic;
using System.Linq;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Models.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ListOfGames.Tests.Compares
{
    /// <summary>
    /// Comparer to check match between two GameDto object
    /// </summary>
    class GameDtoComparer: Comparer<GameDto>
    {
        public override int Compare(GameDto x, GameDto y)
        {
            if (x.Id.CompareTo(y.Id) != 0)
                return -1;

            if (x.Title.CompareTo(y.Title) != 0)
                return -1;

            if (x.Price.CompareTo(y.Price) != 0)
                return -1;

            if (x.Description.CompareTo(y.Description) != 0)
                return -1;

            if (x.Type.CompareTo(y.Type) != 0)
                return -1;
            CollectionAssert.AreEqual(x.PlatformId, y.PlatformId);
            return 0;
        }
    }

}
