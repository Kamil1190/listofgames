﻿using System;
using System.Linq;
using System.Web.Http.Results;
using AutoMapper;
using ListOfGames.App_Start;
using ListOfGames.Controllers;
using ListOfGames.Domain.Abstract;
using ListOfGames.Domain.Entites;
using ListOfGames.Domain.Fake;
using ListOfGames.Domain.Models.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ListOfGames.Tests
{
    /// <summary>
    /// TEST FOR PlatformsController
    /// </summary>
    [TestClass]
    public class PlatformUnitTest
    {

        /// <summary>
        /// Initialize data for test
        /// </summary>
        /// <param name="context">The context of test.</param>
        [ClassInitialize]
        public static void Init(TestContext context)
        {
            //INITIALIZE AUTOMAPPER FOR TEST
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());
        }


        /// <summary>
        /// Test for save new platform
        /// </summary>
        [TestMethod]
        public void Can_Save_Platform()
        {
            Platform platformToAdd = new FakeGameRepository().GetAllPlatforms().FirstOrDefault();
            PlatformDto platformToAddDto = Mapper.Map<PlatformDto>(platformToAdd);

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.SavePlatform(It.IsAny<Platform>())).Returns(platformToAdd);
            PlatformsController controller = new PlatformsController(mock.Object);


            //run
            var result = controller.Post(platformToAddDto);

            //assert
            var response = result as OkNegotiatedContentResult<PlatformDto>;
            Assert.IsNotNull(response);
            mock.Verify(m => m.SavePlatform(It.IsAny<Platform>()));
        }

        /// <summary>
        /// Test for saving new platform when model error occurred
        /// </summary>
        [TestMethod]
        public void Cannot_Save_Platform_Model_Error()
        {
            Platform platformToAdd = new FakeGameRepository().GetAllPlatforms().FirstOrDefault();
            PlatformDto platformToAddDto = Mapper.Map<PlatformDto>(platformToAdd);

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.SavePlatform(It.IsAny<Platform>())).Returns(platformToAdd);
            PlatformsController controller = new PlatformsController(mock.Object);
            controller.ModelState.AddModelError("error", "error");

            //run
            var result = controller.Post(platformToAddDto);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            mock.Verify(m => m.SavePlatform(It.IsAny<Platform>()), Times.Never);
        }

        /// <summary>
        /// Test for update platform
        /// </summary>
        [TestMethod]
        public void Can_Update_Platform()
        {
            Platform platformToUpdate = new FakeGameRepository().GetAllPlatforms().FirstOrDefault();
            PlatformDto platformToUpdateDto = Mapper.Map<PlatformDto>(platformToUpdate);
            int idPlatformToAdd = platformToUpdateDto.Id;

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.UpdatePlatform(It.IsAny<int>(), It.IsAny<Platform>())).Returns(platformToUpdate);
            PlatformsController controller = new PlatformsController(mock.Object);


            //run
            var result = controller.Put(idPlatformToAdd, platformToUpdateDto);

            //assert
            var response = result as OkNegotiatedContentResult<PlatformDto>;
            Assert.IsNotNull(response);
            mock.Verify(m => m.UpdatePlatform(It.Is<int>(i => i == idPlatformToAdd),It.IsAny<Platform>()));
        }

        /// <summary>
        /// Test for update platform when model error occurred
        /// </summary>
        [TestMethod]
        public void Cannot_Update_Platform_Model_Error()
        {
            Platform platformToUpdate = new FakeGameRepository().GetAllPlatforms().FirstOrDefault();
            PlatformDto platformToUpdateDto = Mapper.Map<PlatformDto>(platformToUpdate);
            int idPlatformToAdd = platformToUpdateDto.Id;

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.UpdatePlatform(It.IsAny<int>(), It.IsAny<Platform>())).Returns(platformToUpdate);
            PlatformsController controller = new PlatformsController(mock.Object);
            controller.ModelState.AddModelError("error", "error");

            //run
            var result = controller.Put(idPlatformToAdd, platformToUpdateDto);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            mock.Verify(m => m.UpdatePlatform(It.Is<int>(i => i == idPlatformToAdd), It.IsAny<Platform>()), Times.Never);
        }

        /// <summary>
        /// Test for update platform when bad id occurred
        /// </summary>
        [TestMethod]
        public void Cannot_Update_Platform_Bad_Id()
        {
            Platform platformToUpdate = new FakeGameRepository().GetAllPlatforms().FirstOrDefault();
            PlatformDto platformToUpdateDto = Mapper.Map<PlatformDto>(platformToUpdate);
            int idPlatformToAdd = 0;

            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.UpdatePlatform(It.IsAny<int>(), It.IsAny<Platform>())).Returns(platformToUpdate);
            PlatformsController controller = new PlatformsController(mock.Object);


            //run
            var result = controller.Put(idPlatformToAdd, platformToUpdateDto);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            mock.Verify(m => m.UpdatePlatform(It.Is<int>(i => i == idPlatformToAdd), It.IsAny<Platform>()), Times.Never);
        }
    }
}
