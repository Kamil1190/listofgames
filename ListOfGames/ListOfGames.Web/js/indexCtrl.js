var IndexCtrl = (function () {
    function IndexCtrl($scope, $injector, gamesDataService, gamesTypesDataService) {
        var _this = this;
        this.$scope = $scope;
        this.$injector = $injector;
        this.gamesDataService = gamesDataService;
        this.gamesTypesDataService = gamesTypesDataService;
        this.title = "Games";
        this.loadGames();
        gamesTypesDataService.getGameTypes().then(function (resource) {
            _this.gameTypes = resource.data;
        });
        $scope.$on('gameDataChanged', function () {
            _this.loadGames();
        });
    }
    IndexCtrl.prototype.loadGames = function () {
        this.games = this.gamesDataService.query();
    };
    IndexCtrl.prototype.deleteGame = function (game) {
        var _this = this;
        game.$delete(function () {
            //alert("game deleted");
            _this.loadGames();
        });
    };
    IndexCtrl.prototype.updateGame = function (game) {
        var $state = this.$injector.get('$state');
        $state.transitionTo('update', { 'gameId': game.id });
    };
    return IndexCtrl;
}());
IndexCtrl.$inject = ['$scope', '$injector', 'gamesDataService', 'gamesTypesDataService'];
angular.module('webapp').controller('IndexCtrl', IndexCtrl);
