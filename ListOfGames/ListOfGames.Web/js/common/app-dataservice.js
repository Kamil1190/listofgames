var app;
(function (app) {
    var common;
    (function (common) {
        common.baseAddress = 'http://localhost:51232/';
        var GamesDataService = (function () {
            function GamesDataService($resource) {
                this.$resource = $resource;
                this.dataServiceFactory = {};
                this.updateAction = {
                    method: 'PUT',
                    isArray: false
                };
                // Return the resource, include your custom actions :)
                this.dataServiceFactory = this.$resource(common.baseAddress + 'api/games/:id', { id: '@id' }, {
                    update: this.updateAction
                });
                return this.dataServiceFactory;
            }
            return GamesDataService;
        }());
        GamesDataService.$inject = ['$resource'];
        //register factory in angular module
        angular.module('webapp.resource').factory('gamesDataService', GamesDataService);
    })(common = app.common || (app.common = {}));
})(app || (app = {}));
