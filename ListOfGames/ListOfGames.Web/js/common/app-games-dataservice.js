var app;
(function (app) {
    var common;
    (function (common) {
        common.baseAddress = 'http://localhost:51232/';
        var GamesDataService = (function () {
            function GamesDataService($resource) {
                this.$resource = $resource;
                this.dataServiceFactory = {};
                this.updateAction = {
                    method: 'PUT',
                    isArray: false
                };
                // Return the resource, include your custom actions :)
                this.dataServiceFactory = this.$resource(common.baseAddress + 'api/games/:id', { id: '@id' }, {
                    update: this.updateAction
                });
                return this.dataServiceFactory;
            }
            return GamesDataService;
        }());
        GamesDataService.$inject = ['$resource'];
        common.GamesDataService = GamesDataService;
        var GamesTypesDataService = (function () {
            function GamesTypesDataService($http, $q) {
                this.$http = $http;
                this.$q = $q;
                return this;
            }
            GamesTypesDataService.prototype.getGameTypes = function () {
                var deferred = this.$q.defer();
                this.$http.get(common.baseAddress + 'api/games/types').then(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject(response.data);
                });
                return deferred.promise;
            };
            return GamesTypesDataService;
        }());
        GamesTypesDataService.$inject = ['$http', "$q"];
        //register factory in angular module
        angular.module('webapp.resource').factory('gamesDataService', GamesDataService);
        angular.module('webapp.resource').service('gamesTypesDataService', GamesTypesDataService);
    })(common = app.common || (app.common = {}));
})(app || (app = {}));
