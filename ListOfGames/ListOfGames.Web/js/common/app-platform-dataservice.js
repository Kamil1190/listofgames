var app;
(function (app) {
    var common;
    (function (common) {
        var PlatformDataService = (function () {
            function PlatformDataService($resource) {
                this.$resource = $resource;
                this.dataServiceFactory = {};
                this.updateAction = {
                    method: 'PUT',
                    isArray: false
                };
                this.dataServiceFactory = this.$resource(common.baseAddress + 'api/platforms/:id', { id: '@id' }, {
                    update: this.updateAction
                });
                return this.dataServiceFactory;
            }
            return PlatformDataService;
        }());
        PlatformDataService.$inject = ['$resource'];
        //register in angular module
        angular.module('webapp.resource').factory('platformDataService', PlatformDataService);
    })(common = app.common || (app.common = {}));
})(app || (app = {}));
