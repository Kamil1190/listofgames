var app;
(function (app) {
    angular.module('webapp', ['ui.router', 'webapp.resource', 'ui.bootstrap']);
})(app || (app = {}));
