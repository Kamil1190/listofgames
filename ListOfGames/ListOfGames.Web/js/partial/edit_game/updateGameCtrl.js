var app;
(function (app) {
    var partials;
    (function (partials) {
        var UpdateGameCtrl = (function () {
            function UpdateGameCtrl($injector, $state, gamesDataService, gamesTypesDataService) {
                var _this = this;
                this.$injector = $injector;
                this.$state = $state;
                this.gamesDataService = gamesDataService;
                this.gamesTypesDataService = gamesTypesDataService;
                //load all game types
                gamesTypesDataService.getGameTypes().then(function (response) {
                    _this.gameTypes = response.data;
                    console.log(_this.gameTypes);
                });
                //load game by id in route param
                //var $state = this.$injector.get('$state') as ng.ui.IStateService;
                this.game = gamesDataService.get({ id: $state.params.gameId });
                this.title = "Update game";
            }
            UpdateGameCtrl.prototype.updateGame = function (game) {
                var _this = this;
                game.$save(function (response) {
                    _this.$state.transitionTo('/');
                }, function (response) {
                    _this.title = "błąd dodawania";
                });
            };
            return UpdateGameCtrl;
        }());
        UpdateGameCtrl.$inject = ['$injector', '$state', 'gamesDataService', 'gamesTypesDataService'];
        angular.module('webapp').controller('UpdateGameCtrl', UpdateGameCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
