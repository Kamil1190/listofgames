var app;
(function (app) {
    var partials;
    (function (partials) {
        var UpdateGameCtrl = (function () {
            function UpdateGameCtrl() {
                this.title = "Update game";
            }
            return UpdateGameCtrl;
        }());
        angular.module('webapp').controller('UpdateGameCtrl', UpdateGameCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
