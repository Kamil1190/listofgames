var app;
(function (app) {
    var partials;
    (function (partials) {
        var GameCreateViewModel = (function () {
            function GameCreateViewModel() {
                this.platformId = [];
            }
            return GameCreateViewModel;
        }());
        partials.GameCreateViewModel = GameCreateViewModel;
        var CreateGameCtrl = (function () {
            function CreateGameCtrl($rootScope, $state, gamesDataService, gamesTypesDataService, platformDataService) {
                var _this = this;
                this.$rootScope = $rootScope;
                this.$state = $state;
                this.gamesDataService = gamesDataService;
                this.gamesTypesDataService = gamesTypesDataService;
                this.platformDataService = platformDataService;
                this.title = "Create game";
                this.gameView = new GameCreateViewModel();
                //load all game types
                gamesTypesDataService.getGameTypes().then(function (response) {
                    _this.gameTypes = response.data;
                });
                //load all game platforms
                this.platforms = this.platformDataService.query();
            }
            CreateGameCtrl.prototype.createGame = function (game) {
                var _this = this;
                if (this.form.title.$valid) {
                    this.gamesDataService.save(game, function (response) {
                        _this.$rootScope.$broadcast('gameDataChanged');
                        _this.$state.transitionTo('/');
                    }, function (response) {
                        _this.title = "błąd dodawania";
                    });
                }
            };
            return CreateGameCtrl;
        }());
        CreateGameCtrl.$inject = ['$rootScope', '$state', 'gamesDataService', 'gamesTypesDataService', 'platformDataService'];
        angular.module('webapp').controller('CreateGameCtrl', CreateGameCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
