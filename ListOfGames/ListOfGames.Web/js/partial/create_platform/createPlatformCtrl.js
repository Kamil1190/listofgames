var app;
(function (app) {
    var partials;
    (function (partials) {
        var PlatformCreateViewModel = (function () {
            function PlatformCreateViewModel() {
            }
            return PlatformCreateViewModel;
        }());
        var CreatePlatformCtrl = (function () {
            function CreatePlatformCtrl($rootScope, $state, platformDataService) {
                this.$rootScope = $rootScope;
                this.$state = $state;
                this.platformDataService = platformDataService;
                this.platform = new PlatformCreateViewModel();
                this.title = "Create platform";
            }
            CreatePlatformCtrl.prototype.createPlatform = function (platform) {
                var _this = this;
                if (this.form.name.$valid) {
                    this.platformDataService.save(platform, function () {
                        _this.$rootScope.$broadcast('platformDataChanged');
                        _this.$state.transitionTo('platformList');
                    }, function () {
                        _this.title = "Error";
                    });
                }
            };
            return CreatePlatformCtrl;
        }());
        CreatePlatformCtrl.$inject = ['$rootScope', '$state', 'platformDataService'];
        angular.module('webapp').controller('CreatePlatformCtrl', CreatePlatformCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
