var app;
(function (app) {
    var partials;
    (function (partials) {
        var UpdateGameCtrl = (function () {
            function UpdateGameCtrl($rootScope, $state, gamesDataService, gamesTypesDataService, platformDataService) {
                var _this = this;
                this.$rootScope = $rootScope;
                this.$state = $state;
                this.gamesDataService = gamesDataService;
                this.gamesTypesDataService = gamesTypesDataService;
                this.platformDataService = platformDataService;
                //load all game types
                gamesTypesDataService.getGameTypes().then(function (response) {
                    _this.gameTypes = response.data;
                });
                //load all platforms
                this.platforms = platformDataService.query();
                //load game by id in route param
                this.game = gamesDataService.get({ id: $state.params.gameId });
                this.title = "Update game";
            }
            UpdateGameCtrl.prototype.updateGame = function (game) {
                var _this = this;
                if (this.form.title.$valid) {
                    this.gamesDataService.update(game).$promise.then(function () {
                        _this.$rootScope.$broadcast('gameDataChanged');
                        _this.$state.transitionTo('/');
                    }, function () {
                        _this.title = "Error";
                    });
                }
            };
            return UpdateGameCtrl;
        }());
        UpdateGameCtrl.$inject = ['$rootScope', '$state', 'gamesDataService', 'gamesTypesDataService', 'platformDataService'];
        angular.module('webapp').controller('UpdateGameCtrl', UpdateGameCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
