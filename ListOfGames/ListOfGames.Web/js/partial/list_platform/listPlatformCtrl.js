var app;
(function (app) {
    var partials;
    (function (partials) {
        var ListPlatformCtrl = (function () {
            function ListPlatformCtrl($scope, platformDataService) {
                var _this = this;
                this.$scope = $scope;
                this.platformDataService = platformDataService;
                this.title = "Platforms";
                this.loadPlatforms();
                $scope.$on('platformDataChanged', function () {
                    _this.loadPlatforms();
                });
            }
            ListPlatformCtrl.prototype.loadPlatforms = function () {
                this.platforms = this.platformDataService.query();
            };
            ListPlatformCtrl.prototype.deletePlatform = function (platform) {
                var _this = this;
                platform.$delete(function () {
                    _this.loadPlatforms();
                });
            };
            return ListPlatformCtrl;
        }());
        ListPlatformCtrl.$inject = ['$scope', 'platformDataService'];
        angular.module('webapp').controller('ListPlatformCtrl', ListPlatformCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
