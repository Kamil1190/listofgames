var app;
(function (app) {
    var partials;
    (function (partials) {
        var GameViewModel = (function () {
            function GameViewModel() {
            }
            return GameViewModel;
        }());
        var DetailsGameCtrl = (function () {
            function DetailsGameCtrl($injector, $state, gamesDataService, gamesTypesDataService, platformDataService) {
                var _this = this;
                this.$injector = $injector;
                this.$state = $state;
                this.gamesDataService = gamesDataService;
                this.gamesTypesDataService = gamesTypesDataService;
                this.platformDataService = platformDataService;
                //load all game types
                gamesTypesDataService.getGameTypes().then(function (response) {
                    _this.gameTypes = response.data;
                });
                this.platforms = this.platformDataService.query();
                //load game by id in route param
                this.game = gamesDataService.get({ id: $state.params.gameId });
                this.title = "Game";
            }
            return DetailsGameCtrl;
        }());
        DetailsGameCtrl.$inject = ['$injector', '$state', 'gamesDataService', 'gamesTypesDataService', 'platformDataService'];
        angular.module('webapp').controller('DetailsGameCtrl', DetailsGameCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
