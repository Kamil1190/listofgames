var app;
(function (app) {
    var partials;
    (function (partials) {
        var UpdatePlatformCtrl = (function () {
            function UpdatePlatformCtrl($rootscope, $state, platformDataService) {
                this.$rootscope = $rootscope;
                this.$state = $state;
                this.platformDataService = platformDataService;
                this.title = "Update platform";
                //load platform object
                this.platform = platformDataService.get({ id: $state.params.platformId });
            }
            UpdatePlatformCtrl.prototype.updatePlatform = function (platform) {
                var _this = this;
                if (this.form.name.$valid) {
                    this.platformDataService.update(platform).$promise.then(function () {
                        _this.$rootscope.$broadcast('platformDataChanged');
                        _this.$state.transitionTo('platformList');
                    }, function () {
                        _this.title = "Error";
                    });
                }
            };
            return UpdatePlatformCtrl;
        }());
        UpdatePlatformCtrl.$inject = ['$rootScope', '$state', 'platformDataService'];
        angular.module('webapp').controller('UpdatePlatformCtrl', UpdatePlatformCtrl);
    })(partials = app.partials || (app.partials = {}));
})(app || (app = {}));
