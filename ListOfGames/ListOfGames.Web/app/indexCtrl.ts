class IndexCtrl {

    title: string;
    games: Array<app.models.IGame>;
    gameTypes: Array<app.models.IGameTypes>

    static $inject = ['$scope', '$injector', 'gamesDataService', 'gamesTypesDataService']
    constructor(private $scope: ng.IScope, private $injector: ng.auto.IInjectorService, private gamesDataService: app.common.IGameResource, private gamesTypesDataService: app.common.IGameTypesService) {
        this.title = "Games";
        this.loadGames();

        gamesTypesDataService.getGameTypes().then((resource: any) => {
            this.gameTypes = resource.data;
        });

        $scope.$on('gameDataChanged', () => {
            this.loadGames();
        })
    }

    loadGames(): void {
        this.games = this.gamesDataService.query();
    }

    deleteGame(game: app.models.IGame): void {
        game.$delete(() => {
            //alert("game deleted");
            this.loadGames();
        })
    }

    updateGame(game: app.models.IGame): void {
        var $state = this.$injector.get('$state') as ng.ui.IStateService;
        $state.transitionTo('update', { 'gameId': game.id });
    }

}


angular.module('webapp').controller('IndexCtrl', IndexCtrl);