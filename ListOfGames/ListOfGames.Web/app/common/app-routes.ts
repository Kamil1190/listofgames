module app.common {


    angular.module('webapp').config(routeConfig);

    function routeConfig($stateProvider: ng.ui.IStateProvider,
        $urlRouterProvider: ng.ui.IUrlRouterProvider,
        $locationProvider: ng.ILocationProvider): void {


        $locationProvider.hashPrefix('');
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('/', {
                url: '/',
                templateUrl: '/app/partial/indexContent.html'
            })
            .state('create', {
                url: '/create',
                templateUrl: '/app/partial/create_game/createGame.html',
                controller: 'CreateGameCtrl',
                controllerAs: 'cg'
            })
            .state('update', {
                url: '/update/:gameId',
                templateUrl: '/app/partial/update_game/updateGame.html',
                controller: 'UpdateGameCtrl',
                controllerAs: 'ug'
            })
            .state('gamedetails', {
                url: '/game/:gameId',
                templateUrl: '/app/partial/details_game/detailsGame.html',
                controller: 'DetailsGameCtrl',
                controllerAs: 'dg'
            })
            .state('platformList', {
                url: '/platform',
                templateUrl: '/app/partial/list_platform/listPlatform.html',
                controller: 'ListPlatformCtrl',
                controllerAs: 'lp'
            })
            .state('platformCreate', {
                url: '/platform/create',
                templateUrl: '/app/partial/create_platform/createPlatform.html',
                controller: 'CreatePlatformCtrl',
                controllerAs: 'cp'
            })
            .state('platformUpdate', {
                url: '/platform/update/:platformId',
                templateUrl: '/app/partial/update_platform/updatePlatform.html',
                controller: 'UpdatePlatformCtrl',
                controllerAs: 'up'
            });

    }
}
