module app.common {

    export var baseAddress: string = 'http://localhost:51232/';

    //Games

    export interface IGameResource extends ng.resource.IResourceClass<app.models.IGame> {
        update(IGame: app.models.IGame): app.models.IGame;
    }

    export class GamesDataService {

        dataServiceFactory: any = {};

        static $inject = ['$resource']
        constructor(private $resource: ng.resource.IResourceService) {
            // Return the resource, include your custom actions :)
            this.dataServiceFactory = <IGameResource>this.$resource(baseAddress + 'api/games/:id', { id: '@id' }, {
                update: this.updateAction
            });
            return this.dataServiceFactory;
        }

        updateAction: ng.resource.IActionDescriptor = {
            method: 'PUT',
            isArray: false
        };
 
    }

    //GameTypes

    export interface IGameTypesService{
        getGameTypes(): any;
    }

    class GamesTypesDataService implements IGameTypesService{


        static $inject = ['$http',"$q"]
        constructor(private $http: ng.IHttpService, private $q: ng.IQService){
            return this;
        }

        getGameTypes(): any {
            var deferred = this.$q.defer();
            this.$http.get(baseAddress+'api/games/types').then((response: Array<IGameTypes>) => {
                deferred.resolve(response);
            }, (response: any) => {
                deferred.reject(response.data);
            });
            return deferred.promise;
        }
    }

    //register factory in angular module
    angular.module('webapp.resource').factory('gamesDataService', GamesDataService);
    angular.module('webapp.resource').service('gamesTypesDataService', GamesTypesDataService);

}