module app.common{

    export interface IPlatformResource extends ng.resource.IResourceClass<app.models.IPlatform>{
        update(IPlatform: app.models.IPlatform): app.models.IGame;
    }

    class PlatformDataService{

        dataServiceFactory: any = {};

        static $inject = ['$resource']
        constructor(private $resource: ng.resource.IResourceService) {
            this.dataServiceFactory = <IPlatformResource> this.$resource(baseAddress + 'api/platforms/:id', { id: '@id' }, {
                update: this.updateAction
            });
            return this.dataServiceFactory;
        }

        updateAction: ng.resource.IActionDescriptor = {
            method: 'PUT',
            isArray: false
        };
    }

    //register in angular module
    angular.module('webapp.resource').factory('platformDataService', PlatformDataService);
}