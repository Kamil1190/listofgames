module app.partials {


    class UpdateGameCtrl {

        title: string;
        game: app.models.IGame;
        platforms: Array<app.models.IPlatform>
        gameTypes: Array<app.models.IGameTypes>;
        form: ng.IFormController;

        static $inject = ['$rootScope', '$state', 'gamesDataService', 'gamesTypesDataService', 'platformDataService']
        constructor(private $rootScope: ng.IRootScopeService, private $state: ng.ui.IStateService, private gamesDataService: app.common.IGameResource, private gamesTypesDataService: app.common.IGameTypesService, private platformDataService: app.common.IPlatformResource) {
            //load all game types
            gamesTypesDataService.getGameTypes().then((response: any) => {
                this.gameTypes = response.data;
            });

            //load all platforms
            this.platforms = platformDataService.query();

            //load game by id in route param
            this.game = gamesDataService.get({ id: $state.params.gameId });
            this.title = "Update game";
        }

        updateGame(game: app.models.IGame): void {
            if (this.form.title.$valid) {
                this.gamesDataService.update(game).$promise.then(() => {
                    this.$rootScope.$broadcast('gameDataChanged');
                    this.$state.transitionTo('/');
                }, () => {
                    this.title = "Error"
                });
            }

        }
    }

    angular.module('webapp').controller('UpdateGameCtrl', UpdateGameCtrl);
}
