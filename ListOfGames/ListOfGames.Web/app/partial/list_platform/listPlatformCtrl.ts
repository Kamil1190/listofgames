module app.partials {

    class ListPlatformCtrl {
        
        title: string;
        platforms: Array<app.models.IPlatform>

        static $inject = ['$scope', 'platformDataService']
        constructor(private $scope: ng.IScope, private platformDataService: app.common.IPlatformResource){

            this.title = "Platforms";
            this.loadPlatforms();

            $scope.$on('platformDataChanged', () => {
                this.loadPlatforms();
            });
            
        }

        loadPlatforms(): void {
            this.platforms = this.platformDataService.query();
        }

        deletePlatform(platform: app.models.IPlatform): void {
            platform.$delete(() => {
                this.loadPlatforms();
            });
        }
    }

    angular.module('webapp').controller('ListPlatformCtrl', ListPlatformCtrl);
}