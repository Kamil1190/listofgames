module app.partials {

    class GameViewModel {
        id: number;
        title: string;
        type: string;
        price: number;
        description: string;
    }

    class DetailsGameCtrl {

        title: string;
        game: app.models.IGame;
        gameType: string;
        gameTypes: Array<app.models.IGameTypes>;
        platforms: Array<app.models.IPlatform>;

        static $inject = ['$injector', '$state', 'gamesDataService', 'gamesTypesDataService', 'platformDataService']
        constructor(private $injector: ng.auto.IInjectorService, private $state: ng.ui.IStateService, private gamesDataService: app.common.IGameResource, private gamesTypesDataService: app.common.IGameTypesService, private platformDataService: app.common.IPlatformResource) {
            //load all game types
            gamesTypesDataService.getGameTypes().then((response: any) => {
                this.gameTypes = response.data;
            });

            this.platforms = this.platformDataService.query();

            //load game by id in route param
            this.game = gamesDataService.get({ id: $state.params.gameId });
            this.title = "Game";
        }


    }

    angular.module('webapp').controller('DetailsGameCtrl', DetailsGameCtrl);

}