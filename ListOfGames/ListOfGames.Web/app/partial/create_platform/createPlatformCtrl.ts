module app.partials {

    class PlatformCreateViewModel {
        public id: number;
        public name: string;
    }

    class CreatePlatformCtrl {

        title: string;
        platform: PlatformCreateViewModel;
        form: ng.IFormController;

        static $inject = ['$rootScope', '$state', 'platformDataService']
        constructor(private $rootScope: ng.IRootScopeService, private $state: ng.ui.IStateService, private platformDataService: app.common.IPlatformResource) {

            this.platform = new PlatformCreateViewModel();
            this.title = "Create platform";
        }

        createPlatform(platform: PlatformCreateViewModel): void {
            if (this.form.name.$valid) {
                this.platformDataService.save(platform, () => {
                    this.$rootScope.$broadcast('platformDataChanged');
                    this.$state.transitionTo('platformList');
                }, () => {
                    this.title = "Error";
                });
            }
        }
    }

    angular.module('webapp').controller('CreatePlatformCtrl', CreatePlatformCtrl);
}