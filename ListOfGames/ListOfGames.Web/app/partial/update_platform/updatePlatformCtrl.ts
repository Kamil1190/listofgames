module app.partials {

    class UpdatePlatformCtrl {

        title: string;
        platform: app.models.IPlatform;
        form: ng.IFormController;

        static $inject = ['$rootScope', '$state', 'platformDataService']
        constructor(private $rootscope: ng.IRootScopeService, private $state: ng.ui.IStateService, private platformDataService: app.common.IPlatformResource) {

            this.title = "Update platform";
            //load platform object
            this.platform = platformDataService.get({ id: $state.params.platformId });
        }

        updatePlatform(platform: app.models.IPlatform): void {
            if (this.form.name.$valid) {
                this.platformDataService.update(platform).$promise.then(() => {
                    this.$rootscope.$broadcast('platformDataChanged');
                    this.$state.transitionTo('platformList');
                }, () => {
                    this.title = "Error";
                });
            }
        }
    }

    angular.module('webapp').controller('UpdatePlatformCtrl', UpdatePlatformCtrl);
}