module app.partials {

    export class GameCreateViewModel {
        id: number;
        title: string;
        type: number;
        price: number;
        description: string;
        platformId: Array<number> = [];
    }

    class CreateGameCtrl {

        title: string;
        gameView: GameCreateViewModel;
        gameTypes: Array<app.models.IGameTypes>;
        platforms: Array<app.models.IPlatform>
        form: ng.IFormController;

        static $inject = ['$rootScope', '$state', 'gamesDataService', 'gamesTypesDataService', 'platformDataService']
        constructor(private $rootScope: ng.IRootScopeService, private $state: ng.ui.IStateService, private gamesDataService: app.common.IGameResource, private gamesTypesDataService: app.common.IGameTypesService, private platformDataService: app.common.IPlatformResource) {

            this.title = "Create game";
            this.gameView = new GameCreateViewModel();
            //load all game types
            gamesTypesDataService.getGameTypes().then((response: any) => {
                this.gameTypes = response.data;
            });
            //load all game platforms
            this.platforms = this.platformDataService.query();

        }


        createGame(game: GameCreateViewModel): void {
            if (this.form.title.$valid) {
                this.gamesDataService.save(game, (response: any) => {
                    this.$rootScope.$broadcast('gameDataChanged');
                    this.$state.transitionTo('/');
                }, (response: any) => {
                    this.title = "błąd dodawania";
                });
            }
        }
    }

    angular.module('webapp').controller('CreateGameCtrl', CreateGameCtrl);
}
