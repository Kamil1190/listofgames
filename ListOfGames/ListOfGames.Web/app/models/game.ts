module app.models {

    export interface IGame extends ng.resource.IResource<IGame> {
        id: number;
        title: string;
        type: number;
        price: number;
        description: string;
        platformId: Array<number>;
    }
}