module app.models {

    export interface IGameTypes {
        name: string;
        value: number;
    }

    export class GameTypes implements IGameTypes {
        name: string;
        value: number;
    }
}