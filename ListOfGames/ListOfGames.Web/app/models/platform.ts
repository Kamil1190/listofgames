module app.models {

    export interface IPlatform extends ng.resource.IResource<IPlatform> {
        id: number;
        name: string;
    }

}